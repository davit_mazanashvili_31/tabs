package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.example.myapplication.adapters.ViewPagerFragmentAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {
    private lateinit var ViewPager : ViewPager2
    private lateinit var tabLayout : TabLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ViewPager = findViewById(R.id.viewPager)
        tabLayout = findViewById(R.id.tabLayout)

        ViewPager.adapter = ViewPagerFragmentAdapter(this)
        TabLayoutMediator(tabLayout,ViewPager) {tab, position ->
            if (position == 0 ){
                tab.text = "Pink Floyd"
            }else if(position == 1) {
                tab.text = "Led Zeppelin"
            }else if(position == 2) {
                tab.text = "AC/DC"
            }else if (position ==3 ){
                tab.text = "Guns N' Roses"
            }else{
                tab.text = "Metallica"
            }
        }.attach()
    }
}