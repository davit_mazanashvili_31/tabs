package com.example.myapplication.adapters

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.myapplication.fragments.*

class ViewPagerFragmentAdapter(activity: AppCompatActivity) : FragmentStateAdapter(activity){
    override fun getItemCount() = 5

    override fun createFragment(position: Int): Fragment {
        if(position == 0){
            return fragmentFirst()
        }else if(position ==1){
            return fragmentSecond()
        }else if(position == 2){
            return fragmentThird()
        }else if(position == 3){
            return fragmentFourth()
        }else{
            return fragmentFifth()
        }
    }
}